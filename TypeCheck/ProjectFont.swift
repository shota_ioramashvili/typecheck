enum ProjectFont: String {
    case BPGExcelsior = "BPG Excelsior"
    case BPGExcelsiorLight = "BPG Excelsior Light"
    case BPGPHONESANS = "BPG PHONE SANS"
    case BPGWEB001CAPS = "BPG WEB 001 CAPS"
    case BPGWEB001 = "BPG WEB 001"
}

// გამოყენების მაგალითი

// let font = UIFont(name: ProjectFont.BPGExcelsior.rawValue, size: 15)