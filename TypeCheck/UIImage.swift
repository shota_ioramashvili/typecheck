import UIKit

enum ImageAsset: String {
    case IPNTextLogo = "ipn-text-logo"
    case IPNTextLogoENG = "ipn-text-logo_eng"
    case IPNLogo = "ipn-logo"
    
    // თუ ერთი და იგივე სურათი სხვადასხვა კონტექსტშია გამოსაყენებელი, შეგვიძლია
    // ეს მიდგომა გამოვიყენოთ, რომელიც სხვა არაფერია თუ არა არსებული ქეისის "შეფუთვა"
    // სხვა უფრო მეტად ლოგიკური სახელით
    static var extraMeaning: ImageAsset {
        return .IPNTextLogo
    }
}

extension ImageAsset {
    var image : UIImage {
        return UIImage(named: self.rawValue)!
    }
}

extension UIImage {
    convenience init(asset: ImageAsset) {
        self.init(named: asset.rawValue)!
    }
}



// გამოყენების მაგალითი

// let image1 = UIImage(asset: ImageAsset.IPNTextLogo)
// let image2 = UIImage(asset: ImageAsset.extraMeaning)