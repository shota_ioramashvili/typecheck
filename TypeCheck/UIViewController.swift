import UIKit

extension UIViewController {
    var name: String {
        return String(self)
    }
}

// გამოყენების მაგალითი

//let storyboard = UIStoryboard(name: ProjectStoryboard.Search.rawValue, bundle: nil)
//let chooseLanguage = storyboard.instantiateViewControllerWithIdentifier(SearchViewController.name) as! SearchViewController
