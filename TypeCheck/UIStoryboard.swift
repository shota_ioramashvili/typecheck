import UIKit

extension UIStoryboard {
    convenience init(name: ProjectStoryboard, bundle: NSBundle?) {
        self.init(name: name.rawValue, bundle: bundle)
    }
}