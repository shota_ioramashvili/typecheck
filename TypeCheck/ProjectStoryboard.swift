enum ProjectStoryboard: String {
    case Main = "Main"
    case Setting = "Setting"
    case Search = "Search"
}

// გამოყენების მაგალითი
// let storyboard = UIStoryboard(name: .Main, bundle: nil)